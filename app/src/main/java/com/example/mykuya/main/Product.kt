package com.example.mykuya.main

import java.util.*

data class Product(
    val icon: String,
    val name: String
)