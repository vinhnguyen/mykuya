package com.example.mykuya.main

data class News(
    val title: String,
    val desc: String,
    val cover: String
)