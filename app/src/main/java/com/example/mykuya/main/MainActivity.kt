package com.example.mykuya.main

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.location.Geocoder
import android.os.Bundle
import android.os.Looper
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresPermission
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.text.bold
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.example.mykuya.R
import com.example.mykuya.map.MapActivity
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val requestLocationPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { results ->
            if (results[Manifest.permission.ACCESS_FINE_LOCATION] == true || results[Manifest.permission.ACCESS_COARSE_LOCATION] == true) {
                lifecycleScope.launch(CoroutineExceptionHandler { _, _ -> }) {
                    getLocation()?.let { getAddressAndShow(it) }
                }
            }
        }

    private val pinMapLauncher = registerForActivityResult(object :
        ActivityResultContract<LatLng, LatLng?>() {
        override fun createIntent(context: Context, input: LatLng?): Intent {
            return Intent(context, MapActivity::class.java).apply {
                putExtra(MapActivity.EXTRA_LAT_LNG, input)
            }
        }

        override fun parseResult(resultCode: Int, intent: Intent?): LatLng? {
            return intent?.getParcelableExtra(MapActivity.EXTRA_LAT_LNG)
        }
    }) {
        if (it != null) {
            getAddressAndShow(it)
        }
    }

    private var currentLatLng: LatLng? = null

    private val tvWelcome by lazy { findViewById<TextView>(R.id.tvWelcome) }

    private val tvLocation by lazy { findViewById<TextView>(R.id.tvLocation) }

    private val fusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        tvLocation.setOnClickListener {
            pinMapLauncher.launch(currentLatLng)
        }

        findViewById<RecyclerView>(R.id.rvFeaturedProducts).apply {
            isNestedScrollingEnabled = false
            this.adapter = ProductAdapter().apply {
                submitList(
                    listOf(
                        Product("file:///android_asset/products/2-icon.png", "Cleaning"),
                        Product("file:///android_asset/products/8-icon.png", "Shopping"),
                        Product("file:///android_asset/products/25-icon.png", "Massage"),
                    )
                )
            }
        }

        val products = listOf(
            Product("file:///android_asset/products/2-icon.png", "Cleaning"),
            Product("file:///android_asset/products/3-icon.png", "Event Assistant"),
            Product("file:///android_asset/products/4-icon.png", "Office Assistant"),
            Product("file:///android_asset/products/6-icon.png", "Coffee Delivery"),
            Product("file:///android_asset/products/7-icon.png", "Food Delivery"),
            Product("file:///android_asset/products/8-icon.png", "Shopping"),
            Product("file:///android_asset/products/9-icon.png", "Grocery Delivery"),
            Product("file:///android_asset/products/10-icon.png", "Messenger"),
            Product("file:///android_asset/products/12-icon.png", "Bill Payment"),
            Product("file:///android_asset/products/13-icon.png", "Personal Assistant"),
            Product("file:///android_asset/products/14-icon.png", "Assistant on Bike"),
            Product("file:///android_asset/products/15-icon.png", "Queuing Up"),
            Product("file:///android_asset/products/16-icon.png", "Pet Sitting"),
            Product("file:///android_asset/products/17-icon.png", "Flyeing"),
            Product("file:///android_asset/products/23-icon.png", "Dish Washing"),
            Product("file:///android_asset/products/24-icon.png", "Cash On Delivery"),
            Product("file:///android_asset/products/25-icon.png", "Massage"),
            Product("file:///android_asset/products/26-icon.png", "Deep Clean"),
            Product("file:///android_asset/products/27-icon.png", "Car Wash"),
            Product("file:///android_asset/products/28-icon.png", "Manicure"),
        )
        val adapter = ProductAdapter().apply {
            submitList(products.subList(0, 6))
        }
        val rvProducts = findViewById<RecyclerView>(R.id.rvProducts).apply {
            isNestedScrollingEnabled = false
            this.adapter = adapter
        }
        findViewById<ImageView>(R.id.btExpand).apply {
            setOnClickListener {
                if (tag != true) {
                    tag = true
                    animate().setDuration(200).rotationBy(180F).withEndAction {
                        adapter.submitList(products)
                        rvProducts.invalidate()
                    }
                } else {
                    tag = false
                    animate().setDuration(200).rotationBy(-180F).withEndAction {
                        adapter.submitList(products.subList(0, 6))
                        rvProducts.invalidate()
                    }
                }
            }
        }

        findViewById<RecyclerView>(R.id.rvNews).apply {
            isNestedScrollingEnabled = false
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    outRect.right = resources.getDimensionPixelSize(R.dimen.news_list_spacing)
                }
            })
            this.adapter = NewsAdapter().apply {
                submitList(
                    listOf(
                        News(
                            "How to Use the App",
                            "Getting access to on-demand",
                            "file:///android_asset/news/27.png"
                        ),
                        News(
                            "List your service on MyKuya",
                            "Do You Offer Manpowe",
                            "file:///android_asset/news/29.png"
                        )
                    )
                )
            }
        }

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestLocationPermissionLauncher.launch(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
        } else {
            lifecycleScope.launch {
                getLocation()?.let { getAddressAndShow(it) }
            }
        }

        showTextWelcome("Peyman!")
    }

    private fun showTextWelcome(name: String) {
        tvWelcome.text = SpannableStringBuilder()
            .append("Good Afternoon, ")
            .bold {
                append("$name!")
            }
    }

    @RequiresPermission(anyOf = [Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION])
    private suspend fun getLocation(): LatLng? {
        return suspendCancellableCoroutine { continuation ->
            val request = LocationRequest.create()
                .setFastestInterval(0L)
                .setInterval(0L)
                .setSmallestDisplacement(1000F)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            LocationServices.getSettingsClient(this)
                .checkLocationSettings(
                    LocationSettingsRequest.Builder().addLocationRequest(request).build()
                ).addOnSuccessListener {
                    fusedLocationProviderClient.requestLocationUpdates(
                        request,
                        object : LocationCallback() {
                            override fun onLocationResult(result: LocationResult?) {
                                super.onLocationResult(result)
                                continuation.resume(
                                    result?.locations?.firstOrNull()?.let {
                                        LatLng(it.latitude, it.longitude)
                                    }
                                )
                                fusedLocationProviderClient.removeLocationUpdates(this)
                            }
                        },
                        Looper.getMainLooper()
                    )
                }.addOnFailureListener {
                    if (it is ResolvableApiException) {
                        it.startResolutionForResult(
                            this, REQUEST_CODE_LOCATION_RESOLVABLE
                        )
                    }
                    continuation.resumeWithException(it)
                }
        }
    }

    private fun getAddressAndShow(latLng: LatLng) {
        currentLatLng = latLng
        lifecycleScope.launch(Dispatchers.IO) {
            Geocoder(this@MainActivity, Locale.getDefault()).getFromLocation(
                latLng.latitude,
                latLng.longitude,
                1
            ).let {
                withContext(Dispatchers.Main) {
                    tvLocation.text = it.firstOrNull()?.getAddressLine(0)
                }
            }
        }
    }

    companion object {
        private const val REQUEST_CODE_LOCATION_RESOLVABLE = 1010
    }
}