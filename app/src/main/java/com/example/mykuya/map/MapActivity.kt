package com.example.mykuya.map

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.mykuya.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapActivity : AppCompatActivity(R.layout.activity_map) {


    private var map: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val current = intent.getParcelableExtra<LatLng>(EXTRA_LAT_LNG)
        if (current == null) {
            finish()
            return
        }

        (supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment).getMapAsync {
            this.map = it

            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 17F))

            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return@getMapAsync
            }
            map?.isMyLocationEnabled = true
            map?.uiSettings?.isMyLocationButtonEnabled = true
        }

        findViewById<ImageView>(R.id.btBack).setOnClickListener {
            finish()
        }
        findViewById<Button>(R.id.btConfirm).setOnClickListener {
            map?.cameraPosition?.target?.let {
                setResult(Activity.RESULT_OK, Intent().apply {
                    putExtra(EXTRA_LAT_LNG, it)
                })
                finish()
            }
        }
    }

    companion object {
        const val EXTRA_LAT_LNG = "LatLng"
    }

}